<?php
interface IEntity { //Interface de la que heredan los modelos de la aplicación.
    public function toArray(): array; //Todos los modelos deben contener este método.
}
?>