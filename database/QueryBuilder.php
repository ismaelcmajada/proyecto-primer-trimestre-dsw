<?php

require_once __DIR__ . "/../core/App.php";
require_once __DIR__ . "/../exceptions/QueryException.php";
require_once __DIR__ . "/../database/IEntity.php";

abstract class QueryBuilder //Con esta clase, podemos generar distintos tipos de consultas.
{

    //Propiedades

    private $connection;    //Conexión con la BD
    private $table;         //Tabla de la BD
    private $classEntity;   //Modelo

    //Métodos

    //Constructor

    public function __construct(string $table, string $classEntity)
    {
        $this->connection = App::getConnection(); //Cogemos la conexión del contenedor de la clase APP
        $this->table = $table;
        $this->classEntity = $classEntity;
    }

    //findAll, devuelve todas las tuplas de la tabla, con 2 parámetros opcionales

    public function findAll($parameter1='', $parameter2='')
    {
        $sql = "SELECT * FROM $this->table $parameter1 $parameter2";

        $result = $this->executeQuery($sql);

        return $result;

    }

    //save, crea una nueva tupla con las propiedades de un objeto pasado por parámetro en la BD

    public function save(IEntity $entity): void
    {
        try {
            $parameters = $entity->toArray();  //Usamos el método toArray de la clase, para obtener todos los campos en un array asociativo.
            $sql = sprintf(
                "insert into %s (%s) values (%s)",
                $this->table,
                implode(", ", array_keys($parameters)),
                ":". implode(", :", array_keys($parameters))
            );

            $statement = $this->connection->prepare($sql);

            $statement->execute($parameters);
        } catch (PDOException $exception) {
            throw new QueryException("Error al insertar en la BBDD.");
        }
    }

    //executeQuery, permite pasar una sentencia SQL por parámetro y ejecutarla, cada tupla como un objeto

    public function executeQuery(string $sql): array
    {
        $pdoStatement = $this->connection->prepare($sql);

        if ($pdoStatement->execute()===false) {
            throw new QueryException("No se ha podido ejecutar la consulta");
        }

        return $pdoStatement->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, $this->classEntity);
    }

    //findById, devuelve un objeto con el id pasado por parámetro

    public function findById(int $id): IEntity
    {
        $sql = "SELECT * FROM $this->table WHERE id = $id";

        $result = $this->executeQuery($sql);
        
        return $result[0];
    }

    //deleteById, elimina de la BD la tupla con el id pasado por parámetro

    public function deleteById($id)
    {
        try {
            $sql = "DELETE FROM $this->table where id = :id";

            $pdoStatement = $this->connection->prepare($sql);

            $pdoStatement->execute(array(
                ':id' => $id
            ));
        } catch (PDOException $exception) {
            throw new QueryException("Error al eliminar en la BBDD.");
        }
    }

    //updateById, modifica de la BD la tupla con el id pasado por parámetro, aplicando las propiedades de un objeto, también pasado por parámetro

    public function updateById(IEntity $entity, $id): void
    {
        try {
            $parameters = $entity->toArray(); //Usamos el método toArray de la clase, para obtener todos los campos en un array asociativo.

            foreach ($parameters as $key => $value) {
                $campos[] = "$key = :$key";
            }
    
            $sql = sprintf(
                "update %s set %s where id = %s",
                $this->table,
                implode(', ', $campos),
                $id
            );

            $statement = $this->connection->prepare($sql);

            $statement->execute($parameters);
        } catch (PDOException $exception) {
            throw new QueryException("Error al actualizar en la BBDD.");
        }
    }
}
