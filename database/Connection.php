<?php

require_once __DIR__ . "/../core/App.php";
require_once __DIR__ . "/../exceptions/AppException.php";

class Connection //Con esta clase creamos una conexión con la BD
{
    public static function make()
    {

        $config = App::get("config")["database"]; //Cogemos la configuración del contenedor de la clase APP

        try {

            $connection = new PDO($config["connection"].';dbname='.$config["name"], $config["username"], $config["password"], $config["options"]); //Creamos la conexión
        } catch (PDOException $PDOException) {
            throw new AppException("No se ha podido conectar con la BBDD");
        }
        return $connection; //Devolvemos la conexión
    }
}
?>