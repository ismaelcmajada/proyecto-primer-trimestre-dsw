<?php

//En este archivo almacenamos la lista de rutas amigables de la aplicación

return [

 "proyectoDSW" => "app/controllers/index.php",

 "proyectoDSW/inicio" => "app/controllers/index.php",

 "proyectoDSW/adminposts" => "app/controllers/adminPosts.php",

 "proyectoDSW/admincategories" => "app/controllers/adminCategories.php",

 "proyectoDSW/adminauthors" => "app/controllers/adminAuthors.php",

 "proyectoDSW/adminmessages" => "app/controllers/adminMessages.php",

 "proyectoDSW/createpost" => "app/controllers/createPost.php",

 "proyectoDSW/createcategory" => "app/controllers/createCategory.php",

 "proyectoDSW/createauthor" => "app/controllers/createAuthor.php",

 "proyectoDSW/modifypost" => "app/controllers/modifyPost.php",

 "proyectoDSW/modifycategory" => "app/controllers/modifyCategory.php",

 "proyectoDSW/modifyauthor" => "app/controllers/modifyAuthor.php",

 "proyectoDSW/post" => "app/controllers/post.php",

 "proyectoDSW/about" => "app/controllers/about.php",

 "proyectoDSW/contact" => "app/controllers/contact.php",

];
