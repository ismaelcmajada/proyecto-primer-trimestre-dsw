<?php
include __DIR__ . "/partials/inicio-doc.part.php";
?>

<!-- Navigation -->
<?php
include __DIR__ . "/partials/navAdmin.part.php";
?>

<!-- Page Content -->
<div class="container">

  <div class="row">

    <div class="col">
      <div class="row">
        <h1 class="my-4 text-light">Modifica la publicación</h1>
        <a href="adminposts" class="btn btn-dark m-4"><i class="fas fa-backward align-middle"></i></a>
      </div>

      <?php
        if ($_SERVER['REQUEST_METHOD']==='POST') {
            if (!empty($errores)) {
                foreach ($errores as $error) {
                    echo "<div class='jumbotron bg-danger text-light p-1'>";
                    echo $error;
                    echo "</div>";
                }
            } else {
                echo "<div class='jumbotron bg-success text-light p-1'>";
                echo "Publicación modificada";
                echo "</div>";
            }
        }
        ?>

      <form class="form-horizontal text-light" method="POST" action="modifypost?id=<?=$post->getId();?>"
        enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?=$post->getId()?>">
        <div class="form-group">
          <div class="col-xs-6">
            <label class="label-control">
              <h2>Título</h2>
            </label>
            <input class="form-control mb-4" name="titulo" value="<?=$post->getTitulo();?>" type="text">
            <div class="row">
              <div class="col-12 col-lg-6">
                <label class="label-control">
                  <h2>Autor</h2>
                </label>
                <select class="form-control  mb-4" name="autor">
                  <option value="">Elige una autor</option>
                  <?php
                  foreach($authors ?? [] as $author) {
                  ?>

                  <option value="<?= $author->getId() ?>" <?php if($author->getId()==$post->getAutor()) echo "selected" ?>><?= $author->getNombre() ?></option>

                  <?php
                  }
                  ?>
                </select>
              </div>
              <div class="col-12 col-lg-6">
                <label class="label-control">
                  <h2>Categoría</h2>
                </label>
                <select class="form-control  mb-4" name="categoria">
                  <option value="">Elige una categoría</option>
                  <?php
                  foreach($categories ?? [] as $category) {
                  ?>

                  <option value="<?= $category->getId() ?>" <?php if($category->getId()==$post->getCategoria()) echo "selected" ?>><?= $category->getNombre() ?></option>

                  <?php
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-6">
            <label class="label-control">
              <h2>Contenido</h2>
            </label>
            <textarea class="form-control mb-4" rows="25" name="contenido"><?=$contenido?></textarea>
          </div>
          <h2>Imagen</h2>
          <label class="btn btn-primary btn-file">
            Cambiar Imagen <input type="file" name="imagen" style="display: none;"/>
          </label>
        </div>
        <input type="submit" class="button mb-4 btn btn-success" value="Modificar">
      </form>

    </div>

  </div>
  <!-- /.row -->

</div>
<!-- /.container -->

<?php
include __DIR__ . "/partials/fin-doc.part.php";
?>