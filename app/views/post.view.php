<?php
include __DIR__ . "/partials/inicio-doc.part.php";
?>

<!-- Navigation -->
<?php
include __DIR__ . "/partials/nav.part.php";
?>

<!-- Page Content -->
<div class="container">
  <div class="row">
    <div class="jumbotron p-0 my-4">
      <?php
    if ($post->getImagen() != null) {
    ?>
      <img src="uploads/<?=$post->getImagen(); ?>" class="img-fluid rounded p-1"
        alt="<?=$post->getTitulo(); ?>">
      <?php
    }
    ?>

      <h1 class="m-4 text-dark"><?=$post->getTitulo();?></h1>
      <p class="m-4 text-dark"><?=$post->getContenido();?></p>

      <hr>

      <p class="m-4 text-dark text-center">Publicado el <?=$post->getFecha();?> por <a
          href="about"><?=$postRepository->getAutor($post)->getNombre();?></a></p>

    </div>
  </div>
</div>
<!-- /.container -->

<?php
include __DIR__ . "/partials/fin-doc.part.php";
?>