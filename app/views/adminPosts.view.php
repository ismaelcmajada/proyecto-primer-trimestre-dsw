<?php
include __DIR__ . "/partials/inicio-doc.part.php";
?>

<!-- Navigation -->
<?php
include __DIR__ . "/partials/navAdmin.part.php";
?>

<!-- Page Content -->
<div class="container">

  <h1 class="my-4 text-light">Publicaciones</h1>
    <div class="table-responsive">
      <table class="table table-bordered table-striped table-light">
        <thead class="thead-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Imagen</th>
            <th scope="col">Título</th>
            <th scope="col">Autor</th>
            <th scope="col">Fecha</th>
            <th scope="col">Categoría</th>
            <th colspan="2"><a href="createpost" class="btn btn-success float-right"><i class="fas fa-plus"></i></a>
            </th>
          </tr>
          <thead>
          <tbody>
            <?php
                foreach ($posts ?? [] as $post) {
                    ?>
            <tr>
              <th scope="row"><?=$post->getId()?></th>
              <td><?=$post->getImagen()?></td>
              <td><?=$post->getTitulo()?></td>
              <td><?=$postRepository->getAutor($post)->getNombre()?></td>
              <td><?=$post->getFecha()?></td>
              <td><?=$postRepository->getCategoria($post)->getNombre()?></td>
              <td><a class="btn btn-primary float-right" href="modifypost?id=<?=$post->getId()?>"><i
                    class="fas fa-pencil-alt"></i></a></td>
              <td>
                <form method="POST">
                  <input type="hidden" name="id" value="<?=$post->getId()?>">
                  <input type="submit" class="btn btn-danger float-right" name="delete" value="Eliminar">
                </form>
              </td>
            </tr>
            <?php
                }
            ?>
          </tbody>
      </table>
    </div>
</div>

</div>
<!-- /.row -->

</div>
<!-- /.container -->

<?php
include __DIR__ . "/partials/fin-doc.part.php";
?>