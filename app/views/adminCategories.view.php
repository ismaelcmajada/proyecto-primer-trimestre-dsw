<?php
include __DIR__ . "/partials/inicio-doc.part.php";
?>

<!-- Navigation -->
<?php
include __DIR__ . "/partials/navAdmin.part.php";
?>

<!-- Page Content -->
<div class="container">

  <h1 class="my-4 text-light">Categorías</h1>
    <div class="table-responsive">
      <table class="table table-bordered table-striped table-light">
        <thead class="thead-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th colspan="2"><a href="createcategory" class="btn btn-success float-right"><i class="fas fa-plus"></i></a>
            </th>
          </tr>
          <thead>
          <tbody>
            <?php
                foreach ($categories ?? [] as $category) {
                    ?>
            <tr>
              <th scope="row"><?=$category->getId()?></th>
              
              <td><?=$category->getNombre()?></td>
              <td><a class="btn btn-primary float-right" href="modifycategory?id=<?=$category->getId()?>"><i
                    class="fas fa-pencil-alt"></i></a></td>
              <td>
                <form method="POST">
                  <input type="hidden" name="id" value="<?=$category->getId()?>">
                  <input type="submit" class="btn btn-danger float-right" name="delete" value="Eliminar">
                </form>
              </td>
            </tr>
            <?php
                }
            ?>
          </tbody>
      </table>
    </div>
</div>

</div>
<!-- /.row -->

</div>
<!-- /.container -->

<?php
include __DIR__ . "/partials/fin-doc.part.php";
?>