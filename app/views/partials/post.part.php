<?php 
foreach ($posts ?? [] as $post) {
?>
<div class="card mb-4">
    <?php
    if ($post->getImagen() != null) {
    ?>
    <img class="card-img-top" src="uploads/<?=$post->getImagen()?>" alt="Card image cap">
    <?php
    }
    ?>
    <div class="card-body">
        <h2 class="card-title"><?=$post->getTitulo()?></h2>
        <p class="card-text"><?=limitText($post->getContenido(),50)?></p>
        <div class="row">
            <a href="post?id=<?=$post->getId()?>" class="btn btn-primary mr-auto">Leer más &rarr;</a>

            <div class="btn btn-primary"><?=$postRepository->getCategoria($post)->getNombre()?></div>
        </div>
    </div>
    <div class="card-footer text-muted">
        Publicado el <?=$post->getFecha()?> por
        <a href="about"><?=$postRepository->getAutor($post)->getNombre()?></a>
    </div>
</div>
<?php
}
?>