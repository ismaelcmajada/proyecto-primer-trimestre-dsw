<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="inicio">Blog Ismael</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item <?php if(active("inicio")) echo "active"; ?>">
            <a class="nav-link" href="inicio">Inicio</a>
          </li>
          <li class="nav-item <?php if(active("contact")) echo "active"; ?>">
            <a class="nav-link" href="contact">Contacto</a>
          </li>
          <li class="nav-item <?php if(active("about")) echo "active"; ?>">
            <a class="nav-link" href="about">Sobre nosotros</a>
          </li>
          <li class="nav-item <?php if(active("admin")) echo "active"; ?>">
            <a class="nav-link" href="adminposts">Administración</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>