<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="inicio">Volver</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item <?php if(active("post")) echo "active"; ?>">
            <a class="nav-link" href="adminposts">Publicaciones</a>
          </li>
          <li class="nav-item <?php if(active("categor")) echo "active"; ?>">
            <a class="nav-link" href="admincategories">Categorías</a>
          </li>
          <li class="nav-item <?php if(active("author")) echo "active"; ?>">
            <a class="nav-link" href="adminauthors">Autores</a>
          </li>
          <li class="nav-item <?php if(active("message")) echo "active"; ?>">
            <a class="nav-link" href="adminmessages">Mensajes</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>