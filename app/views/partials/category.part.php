<div class="card my-4">
    <h5 class="card-header">Categorías</h5>
    <div class="card-body">
        <div class="row">
            <?php
            foreach($categories ?? [] as $category) {
            ?>

            <div class="col-6">
                <a href="inicio?categoria=<?= $category->getId() ?>"><?= $category->getNombre() ?></a>
            </div>

            <?php
            }
            ?>
        </div>
    </div>
</div>