<?php 
foreach ($authors ?? [] as $author) {
?>
<div class="col-md-4">
    <div class="card mb-4">
        <?php
        if ($author->getImagen() != null) {
        ?>
        <img class="card-img-top" src="uploads/<?=$author->getImagen()?>" alt="Card image cap">
        <?php
        }
        ?>
        <div class="card-body">
            <h2 class="card-title"><?=$author->getNombre().' '.$author->getApellidos()?></h2>
            <p class="card-text"><?=$author->getDescripcion()?></p>
        </div>
    </div>
</div>
<?php
}
?>