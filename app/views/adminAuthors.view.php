<?php
include __DIR__ . "/partials/inicio-doc.part.php";
?>

<!-- Navigation -->
<?php
include __DIR__ . "/partials/navAdmin.part.php";
?>

<!-- Page Content -->
<div class="container">

  <h1 class="my-4 text-light">Autores</h1>
    <div class="table-responsive">
      <table class="table table-bordered table-striped table-light">
        <thead class="thead-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Imagen</th>
            <th scope="col">Nombre</th>
            <th scope="col">Apellidos</th>
            <th colspan="2"><a href="createauthor" class="btn btn-success float-right"><i class="fas fa-plus"></i></a>
            </th>
          </tr>
          <thead>
          <tbody>
            <?php
                foreach ($authors ?? [] as $author) {
                    ?>
            <tr>
              <th scope="row"><?=$author->getId()?></th>
              <td><?=$author->getImagen()?></td>
              <td><?=$author->getNombre()?></td>
              <td><?=$author->getApellidos()?></td>
              <td><a class="btn btn-primary float-right" href="modifyauthor?id=<?=$author->getId()?>"><i
                    class="fas fa-pencil-alt"></i></a></td>
              <td>
                <form method="POST">
                  <input type="hidden" name="id" value="<?=$author->getId()?>">
                  <input type="submit" class="btn btn-danger float-right" name="delete" value="Eliminar">
                </form>
              </td>
            </tr>
            <?php
                }
            ?>
          </tbody>
      </table>
    </div>
</div>

</div>
<!-- /.row -->

</div>
<!-- /.container -->

<?php
include __DIR__ . "/partials/fin-doc.part.php";
?>