<?php
include __DIR__ . "/partials/inicio-doc.part.php";
?>

<!-- Navigation -->
<?php
include __DIR__ . "/partials/nav.part.php";
?>

<!-- Page Content -->
<div class="container">

  <div class="row">

    <!-- Blog Entries Column -->
    <div class="col-md-8">

      <h1 class="my-4 text-light">Publicaciones</h1>

      <!-- Blog Post -->
      <?php
      include __DIR__ . "/partials/post.part.php";
      ?>

    </div>

    <!-- Sidebar Widgets Column -->
    <div class="col-md-4">

      <!-- Categories Widget -->
      <?php
      include __DIR__ . "/partials/category.part.php";
      ?>

    </div>

  </div>
  <!-- /.row -->

</div>
<!-- /.container -->

<?php
include __DIR__ . "/partials/fin-doc.part.php";
?>