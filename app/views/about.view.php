<?php
include __DIR__ . "/partials/inicio-doc.part.php";
?>

<!-- Navigation -->
<?php
include __DIR__ . "/partials/nav.part.php";
?>

<!-- Page Content -->
<div class="container">


  <!-- Blog Entries Column -->
  <div class="col-xs-6">
    <div class="row">
      <div class="col">
        <h1 class="my-4 text-light">Nuestros autores</h1>
      </div>
    </div>

    <!-- Authors -->
    <div class="row">
      <?php
        include __DIR__ . "/partials/author.part.php";
        ?>
    </div>
  </div>

</div>
<!-- /.row -->

</div>
<!-- /.container -->

<?php
include __DIR__ . "/partials/fin-doc.part.php";
?>