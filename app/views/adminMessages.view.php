<?php
include __DIR__ . "/partials/inicio-doc.part.php";
?>

<!-- Navigation -->
<?php
include __DIR__ . "/partials/navAdmin.part.php";
?>

<!-- Page Content -->
<div class="container">

  <h1 class="my-4 text-light">Mensajes</h1>
    <div class="table-responsive">
      <table class="table table-bordered table-striped table-light">
        <thead class="thead-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th scope="col">Apellidos</th>
            <th scope="col">Email</th>
            <th scope="col">Asunto</th>
            <th scope="col">Mensaje</th>
            <th></th>
          </tr>
          <thead>
          <tbody>
            <?php
                foreach ($messages ?? [] as $message) {
                    ?>
            <tr>
              <th scope="row"><?=$message->getId()?></th>
              <td><?=$message->getNombre()?></td>
              <td><?=$message->getApellidos()?></td>
              <td><?=$message->getEmail()?></td>
              <td><?=$message->getAsunto()?></td>
              <td><?=$message->getMensaje()?></td>
              <td>
                <form method="POST">
                  <input type="hidden" name="id" value="<?=$message->getId()?>">
                  <input type="submit" class="btn btn-danger float-right" name="delete" value="Eliminar">
                </form>
              </td>
            </tr>
            <?php
                }
            ?>
          </tbody>
      </table>
    </div>
</div>

</div>
<!-- /.row -->

</div>
<!-- /.container -->

<?php
include __DIR__ . "/partials/fin-doc.part.php";
?>