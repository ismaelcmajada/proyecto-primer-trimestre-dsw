<?php
include __DIR__ . "/partials/inicio-doc.part.php";
?>

<!-- Navigation -->
<?php
include __DIR__ . "/partials/navAdmin.part.php";
?>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col">
            <div class="row">
                <h1 class="my-4 text-light">Modifica la Categoría</h1>
                <a href="admincategories" class="btn btn-dark m-4"><i class="fas fa-backward align-middle"></i></a>
            </div>

            <?php
        if ($_SERVER['REQUEST_METHOD']==='POST') {
            if (!empty($errores)) {
                foreach ($errores as $error) {
                    echo "<div class='jumbotron bg-danger text-light p-1'>";
                    echo $error;
                    echo "</div>";
                }
            } else {
                echo "<div class='jumbotron bg-success text-light p-1'>";
                echo "Publicación modificada";
                echo "</div>";
            }
        }
        ?>

            <form class="form-horizontal text-light" method="POST" action="modifycategory?id=<?=$category->getId();?>"
                enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?=$category->getId()?>">
                <div class="form-group">
                    <div class="col-xs-6">
                        <label class="label-control">
                            <h2>Nombre</h2>
                        </label>
                        <input class="form-control mb-4" name="nombre" value="<?=$category->getNombre();?>" type="text">
                    </div>
                </div>
                <input type="submit" class="button mb-4 btn btn-success" value="Modificar">
            </form>

        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->

<?php
include __DIR__ . "/partials/fin-doc.part.php";
?>