<?php
include __DIR__ . "/partials/inicio-doc.part.php";
?>

<!-- Navigation -->
<?php
include __DIR__ . "/partials/navAdmin.part.php";
?>

<!-- Page Content -->
<div class="container">

  <div class="row">

    <div class="col">
      <div class="row">

        <h1 class="my-4 text-light">Crea una publicación</h1>
        <a href="adminposts" class="btn btn-dark m-4"><i class="fas fa-backward align-middle"></i></a>

      </div>

      <?php
      if (!empty($errores)) {
        foreach ($errores as $error) {
          echo "<div class='jumbotron bg-danger text-light p-1'>";
          echo $error;
          echo "</div>";
        }
      } elseif ($_SERVER['REQUEST_METHOD']==='POST') {
        echo "<div class='jumbotron bg-success text-light p-1'>";
        echo "Publicación creada";
        echo "</div>";
      }
      ?>

      <form class="form-horizontal text-light" method="POST" enctype="multipart/form-data">
        <div class="form-group">
          <div class="col-xs-6">
            <label class="label-control">
              <h2>Título</h2>
            </label>
            <input class="form-control mb-4" name="titulo" value="<?=$_POST["titulo"] ?? "" ?>" type="text">
            <div class="row">
              <div class="col-12 col-lg-6">
                <label class="label-control">
                  <h2>Autor</h2>
                </label>
                <select class="form-control  mb-4" name="autor">
                  <option value="">Elige un autor</option>
                  <?php
                  foreach ($authors ?? [] as $author) {
                      ?>

                  <option value="<?= $author->getId() ?>" <?php if (isset($_POST["autor"]) && $author->getId()==$_POST["autor"]) {
                          echo "selected";
                      } ?>>
                    <?= $author->getNombre() ?></option>

                  <?php
                  }
                  ?>
                </select>
              </div>
              <div class="col-12 col-lg-6">
                <label class="label-control">
                  <h2>Categoría</h2>
                </label>
                <select class="form-control  mb-4" name="categoria">
                  <option value="">Elige una categoría</option>
                  <?php
                  foreach ($categories ?? [] as $category) {
                      ?>

                  <option value="<?= $category->getId() ?>" <?php if (isset($_POST["categoria"]) && $category->getId()==$_POST["categoria"]) {
                          echo "selected";
                      } ?>>
                    <?= $category->getNombre() ?></option>

                  <?php
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-6">
            <label class="label-control">
              <h2>Contenido</h2>
            </label>
            <textarea class="form-control mb-4" rows="25" name="contenido"><?=$_POST["contenido"] ?? "" ?></textarea>
          </div>
          <label class="label-control">
            <h2>Imagen</h2>
          </label>
          <input class="form-control-file mb-4 btn btn-primary" name="imagen" type="file">
        </div>
        <input type="submit" class="button mb-4 btn btn-success" value="Crear">
      </form>

    </div>

  </div>
  <!-- /.row -->

</div>
<!-- /.container -->

<?php
include __DIR__ . "/partials/fin-doc.part.php";
?>