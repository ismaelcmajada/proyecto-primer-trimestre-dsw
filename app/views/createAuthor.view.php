<?php
include __DIR__ . "/partials/inicio-doc.part.php";
?>

<!-- Navigation -->
<?php
include __DIR__ . "/partials/navAdmin.part.php";
?>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col">
            <div class="row">

                <h1 class="my-4 text-light">Crea una autor</h1>
                <a href="adminauthors" class="btn btn-dark m-4"><i class="fas fa-backward align-middle"></i></a>

            </div>

            <?php
      if (!empty($errores)) {
          foreach ($errores as $error) {
              echo "<div class='jumbotron bg-danger text-light p-1'>";
              echo $error;
              echo "</div>";
          }
      } elseif ($_SERVER['REQUEST_METHOD']==='POST') {
          echo "<div class='jumbotron bg-success text-light p-1'>";
          echo "Publicación creada";
          echo "</div>";
      }
      ?>
            <form class="form-horizontal text-light" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <div class="col-xs-6">
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <label class="label-control">
                                    <h2>Nombre</h2>
                                </label>
                                <input class="form-control mb-4" name="nombre" value="<?=$_POST["nombre"] ?? "" ?>"
                                    type="text">
                            </div>
                            <div class="col-12 col-lg-6">
                                <label class="label-control">
                                    <h2>Apellidos</h2>
                                </label>
                                <input class="form-control mb-4" name="apellidos"
                                    value="<?=$_POST["apellidos"] ?? "" ?>" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <label class="label-control">
                            <h2>Descripción</h2>
                        </label>
                        <textarea class="form-control mb-4" rows="15"
                            name="descripcion"><?=$_POST["descripcion"] ?? "" ?></textarea>
                    </div>
                    <label class="label-control">
                        <h2>Imagen</h2>
                    </label>
                    <input class="form-control-file mb-4 btn btn-primary" name="imagen" type="file">
                </div>
                <input type="submit" class="button mb-4 btn btn-success" value="Crear">
            </form>

        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->

<?php
include __DIR__ . "/partials/fin-doc.part.php";
?>