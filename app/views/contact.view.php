<?php
include __DIR__ . "/partials/inicio-doc.part.php";
?>

<!-- Navigation -->
<?php
include __DIR__ . "/partials/nav.part.php";
?>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col">

            <h1 class="my-4 text-light">Envianos un mensaje</h1>


            <?php
        if ($_SERVER['REQUEST_METHOD']==='POST') {
            if (!empty($errores)) {
                foreach ($errores as $error) {
                    echo "<div class='jumbotron bg-danger text-light p-1'>";
                    echo $error;
                    echo "</div>";
                }
            } else {
                echo "<div class='jumbotron bg-success text-light p-1'>";
                echo "Mensaje enviado";
                echo "</div>";
            }
        }
        ?>

            <form class="form-horizontal text-light" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <div class="col-xs-6">
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <label class="label-control">
                                    <h2>Nombre</h2>
                                </label>
                                <input class="form-control mb-4" name="nombre" value="<?=$_POST["nombre"] ?? "" ?>"
                                    type="text">
                            </div>
                            <div class="col-12 col-lg-6">
                                <label class="label-control">
                                    <h2>Apellidos</h2>
                                </label>
                                <input class="form-control mb-4" name="apellidos"
                                    value="<?=$_POST["apellidos"] ?? "" ?>" type="text">
                            </div>
                        </div>
                        <label class="label-control">
                            <h2>Email</h2>
                        </label>
                        <input class="form-control mb-4" name="email" value="<?=$_POST["email"] ?? "" ?>" type="text">
                        <label class="label-control">
                            <h2>Asunto</h2>
                        </label>
                        <input class="form-control mb-4" name="asunto" value="<?=$_POST["asunto"] ?? "" ?>" type="text">
                    </div>
                    <div class="col-xs-6">
                        <label class="label-control">
                            <h2>Mensaje</h2>
                        </label>
                        <textarea class="form-control mb-4" name="mensaje"><?=$_POST["mensaje"] ?? "" ?></textarea>
                    </div>
                </div>
                <input type="submit" class="button mb-4 btn btn-success" value="Enviar">
            </form>

        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->

<?php
include __DIR__ . "/partials/fin-doc.part.php";
?>