<?php
  require "utils/utils.php";
  require "utils/File.php";
  require "repository/AuthorRepository.php";

  $errores = [];

  //Validación del formulario.
  if ($_SERVER['REQUEST_METHOD']==='POST') {
      if (empty($_POST["nombre"])) {
          array_push($errores, "El campo nombre es obligatorio");
      }

      if (empty($_POST["apellidos"])) {
          array_push($errores, "El campo apellidos es obligatorio");
      }
 
      if (empty($_POST["descripcion"])) {
          array_push($errores, "El campo descripcion es obligatorio");
      }
  }

  //Inserción de datos.

    try {

        //Conexión con la BD

        $connection = App::getConnection();

        //Repositorios

        $authorRepository = new AuthorRepository();
     
        if ($_SERVER["REQUEST_METHOD"]==="POST") {
            if ($_FILES["imagen"]["name"] != null) { //Comprobamos si se ha enviado una imagen en el formulario.
                $tiposAceptados = ["image/jpeg", "image/jpg", "image/png", "image/gif"];
                $imagen = new File("imagen", $tiposAceptados); //Creamos un objeto File con la imagen enviada.

                $imagen->moveUploadFile("uploads/"); //Movemos la imagen a la carpeta uploads

                $nombreImagen = $imagen->getFileName();
            }

            $descripcion = htmlspecialchars($_POST["descripcion"]); //Evitamos la inyección html en descripcion.
        
            if (empty($errores)) { //Si no hay errores en el formulario, creamos un nuevo author y lo metemos en la BD
                $author = new Author(
                    $nombreImagen ?? null, //En caso de que no se haya enviado una imagen, se guarda como null
                    htmlspecialchars($_POST["nombre"]), 
                    htmlspecialchars($_POST["apellidos"]), 
                    nl2br($descripcion) //Con esta función podemos mantener los saltos de línea del textarea mediante la generación de <br/>
                );

                $authorRepository->save($author); //Guardamos el autor
            }
        }

        //Excepciones

    } catch (FileException $fileException) {
        $errores [] = $fileException->getMessage();
    } catch (AppException $appException) {
        $errores [] = $appException->getMessage();
    } catch (QueryException $queryException) {
        $errores [] = $queryException->getMessage();
    }

    //Vista
  
  require __DIR__ . "/../views/createAuthor.view.php";
