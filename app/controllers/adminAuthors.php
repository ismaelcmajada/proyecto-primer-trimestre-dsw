<?php
  require "utils/utils.php";
  require "repository/AuthorRepository.php";

  try {

    //Conexión con la BD

      $connection = App::getConnection();

      //Repositorios

      $authorRepository = new AuthorRepository();

      //Consulta de los autores

      $authors = $authorRepository->findAll();

      //Eliminar autor por formulario

      if ($_SERVER['REQUEST_METHOD']==='POST') {
          if (isset($_POST['delete']) && isset($_POST['id'])) {

              $author = $authorRepository->findById($_POST['id']);
   
              unlink("uploads/".$author->getImagen()); //Se elimina la imagen asociada al autor

              $authorRepository->deleteById($_POST['id']);

              header("Location: adminauthors"); //Despues de eliminar al autor, recargamos el archivo
          }
      }

      //Excepciones

  } catch (AppException $appException) {
    $errores [] = $appException->getMessage();
  } catch (QueryException $queryException) {
    $errores [] = $queryException->getMessage();
  }
  
  //Vista

  require __DIR__ . "/../views/adminAuthors.view.php";