<?php
  require "utils/utils.php";
  require "repository/MessageRepository.php";

  try {

    //Conexión con la BD

      $connection = App::getConnection();

      //Repositorios

      $messageRepository = new MessageRepository();

      //Consulta de los mensajes

      $messages = $messageRepository->findAll();

      //Eliminar mensajes por formulario

      if ($_SERVER['REQUEST_METHOD']==='POST') {
          if (isset($_POST['delete']) && isset($_POST['id'])) {

              $message = $messageRepository->findById($_POST['id']);

              $messageRepository->deleteById($_POST['id']);

              header("Location: adminmessages"); //Al eliminar el mensaje se recarga el archivo
          }
      }

      //Excepciones
      
  } catch (AppException $appException) {
    $errores [] = $appException->getMessage();
  } catch (QueryException $queryException) {
    $errores [] = $queryException->getMessage();
  }

  //Vista
  
  require __DIR__ . "/../views/adminMessages.view.php";