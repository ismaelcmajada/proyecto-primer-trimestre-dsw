<?php
  require "utils/utils.php";
  require "repository/AuthorRepository.php";
  
  try {

    //Conexión con la BD

      $connection = App::getConnection();

      //Repositorios

      $authorRepository = new AuthorRepository();

      //Consulta de los autores

      $authors = $authorRepository->findAll();

      //Excepciones

  } catch (AppException $appException) {
      $errores [] = $appException->getMessage();
  } catch (QueryException $queryException) {
    $errores [] = $queryException->getMessage();
  }

  //Vista
  
  require __DIR__ . "/../views/about.view.php";
