<?php
  require "utils/utils.php";
  require "repository/PostRepository.php";

  try {

    //Conexión con la BD

      $connection = App::getConnection();

      //Repositorios

      $postRepository = new PostRepository();

      //Consulta de los posts

      $posts = $postRepository->findAll();

      //Eliminar post por formulario

      if ($_SERVER['REQUEST_METHOD']==='POST') {
          if (isset($_POST['delete']) && isset($_POST['id'])) {

              $post = $postRepository->findById($_POST['id']);
   
              unlink("uploads/".$post->getImagen()); //Eliminamos la imagen asociada al post

              $postRepository->deleteById($_POST['id']);

              header("Location: adminposts"); //Al eliminar el post, se recarga el archivo
          }
      }

      //Excepciones

  } catch (AppException $appException) {
    $errores [] = $appException->getMessage();
  } catch (QueryException $queryException) {
    $errores [] = $queryException->getMessage();
  }

  //Vista

  require __DIR__ . "/../views/adminPosts.view.php";