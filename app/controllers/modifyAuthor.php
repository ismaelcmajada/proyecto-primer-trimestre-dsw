<?php
  require "utils/utils.php";
  require "utils/File.php";
  require "repository/AuthorRepository.php";

  $errores = [];

  //Validación del formulario.
  if ($_SERVER['REQUEST_METHOD']==='POST') {
    if (empty($_POST["nombre"])) {
        array_push($errores, "El campo nombre es obligatorio");
    }

    if (empty($_POST["apellidos"])) {
        array_push($errores, "El campo apellidos es obligatorio");
    }

    if (empty($_POST["descripcion"])) {
        array_push($errores, "El campo descripcion es obligatorio");
    }
  }

  //Modificación de datos.
    try {

        //Conexión con la BD

        $connection = App::getConnection();

        //Repositorios

        $authorRepository = new AuthorRepository();

        //Consulta del author a modificar

        $author = $authorRepository->findById($_GET["id"]);

        $descripcion = str_replace("<br />", "", $author->getDescripcion()); //Quitamos los <br/> generados en la descripcion

        if ($_SERVER["REQUEST_METHOD"]==="POST") {
            if ($_FILES["imagen"]["name"] != null) { //Comprobamos si se ha enviado una imagen por el formulario
                $tiposAceptados = ["image/jpeg", "image/jpg", "image/png", "image/gif"];
                $imagen = new File("imagen", $tiposAceptados); //Creamos un objeto File con la imagen

                $imagen->moveUploadFile("uploads/"); //Movemos la imagen a la carpeta uploads

                $nombreImagen = $imagen->getFileName();

                if ($author->getImagen()!=null) { //En caso de que el author ya tuviera una imagen asociada previamente, se elimina la imagen anterior.
                    unlink("uploads/".$author->getImagen());
                }

            } else { //En caso de que no se envie una imagen por el formulario, se mantiene la que tenía previamente.
                $nombreImagen = $author->getImagen();
            }

            $descripcion = htmlspecialchars($_POST["descripcion"]); //Evitamos la inyección html en la descripcion
        
            if (empty($errores)) { //Si no hay errores en el formulario, se crea un nuevo author.

                $authorUpdate = new Author(
                    $nombreImagen ?? null, //Si no hay imagen, se almacena como null
                    htmlspecialchars($_POST["nombre"]), 
                    htmlspecialchars($_POST["apellidos"]), 
                    nl2br($descripcion) //Con esta función mantenemos los saltos de línea del text area mediante la generación de <br/>
                );

                $authorRepository->updateById($authorUpdate, $_POST["id"]); //Modificamos el author con el id pasado por POST, usando las propiedades del author que hemos creado.

            }
        }

        //Excepciones

    } catch (FileException $fileException) {
        $errores [] = $fileException->getMessage();
    } catch (AppException $appException) {
        $errores [] = $appException->getMessage();
    } catch (QueryException $queryException) {
        $errores [] = $queryException->getMessage();
    }

    //Vista
  
  require __DIR__ . "/../views/modifyAuthor.view.php";
