<?php
  require "utils/utils.php";
  require "utils/File.php";
  require "repository/PostRepository.php";

  $errores = [];

  //Validación del formulario
  if ($_SERVER['REQUEST_METHOD']==='POST') {
      if (empty($_POST["titulo"])) {
          array_push($errores, "El campo titulo es obligatorio");
      }

      if (empty($_POST["autor"])) {
          array_push($errores, "El campo autor es obligatorio");
      }
 
      if (empty($_POST["contenido"])) {
          array_push($errores, "El campo contenido es obligatorio");
      }

      if (empty($_POST["categoria"])) {
          array_push($errores, "El campo categoría es obligatorio");
      }
  }

  //Modificación de datos
    try {

        //Conexión con la BD

        $connection = App::getConnection();

        //Repositorios

        $postRepository = new PostRepository();

        $categoryRepository = new CategoryRepository();

        $authorRepository = new AuthorRepository();

        //Consulta del post a modificar

        $post = $postRepository->findById($_GET["id"]);

        //Consulta de las categorías

        $categories = $categoryRepository->findAll();

        //Consulta de los autores

        $authors = $authorRepository->findAll();

        $contenido = str_replace("<br />", "", $post->getContenido()); //Quitamos los <br/> generados del contenido

        if ($_SERVER["REQUEST_METHOD"]==="POST") {
            if ($_FILES["imagen"]["name"] != null) { //Comprobamos si se ha enviado una imagen por el formulario.
                $tiposAceptados = ["image/jpeg", "image/jpg", "image/png", "image/gif"];
                $imagen = new File("imagen", $tiposAceptados); //Creamos un objeto File con la imagen

                $imagen->moveUploadFile("uploads/"); //Movemos la imagen a la carpeta uploads

                $nombreImagen = $imagen->getFileName();

                if ($post->getImagen()!=null) { //En caso de que el post ya tuviera una imagen asociada, se elimina la anterior
                    unlink("uploads/".$post->getImagen());
                }

            } else { //En caso de que no se envie una imagen por el formulario, se mantiene la imagen que tenía anteriormente.
                $nombreImagen = $post->getImagen();
            }

            $contenido = htmlspecialchars($_POST["contenido"]); //Evitamos la inyección html del contenido
        
            if (empty($errores)) { //Si no hay errores en el formulario, creamos un nuevo post

                $postUpdate = new Post(
                    htmlspecialchars($_POST["titulo"]),
                    nl2br($contenido), //Con esta función mantenemos los saltod de línea del textarea mediante la generación de <br/>
                    $nombreImagen ?? null, //Si no hay una imagen, se almacena como null
                    htmlspecialchars($_POST["autor"]),
                    htmlspecialchars($_POST["categoria"])
                );

                $postRepository->updateById($postUpdate, $_POST["id"]); //Modificamos el post con el id pasado por POST, usando las propiedades del post que hemos creado

            }
        }

        //Excepciones

    } catch (FileException $fileException) {
        $errores [] = $fileException->getMessage();
    } catch (AppException $appException) {
        $errores [] = $appException->getMessage();
    } catch (QueryException $queryException) {
        $errores [] = $queryException->getMessage();
    }

    //Vista
  
  require __DIR__ . "/../views/modifyPost.view.php";
