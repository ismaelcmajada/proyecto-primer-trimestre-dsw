<?php
  require "utils/utils.php";
  require "utils/File.php";
  require "repository/PostRepository.php";

  $errores = [];

  //Validación del formulario.
  if ($_SERVER['REQUEST_METHOD']==='POST') {

      if (empty($_POST["nombre"])) {
          array_push($errores, "El campo nombre es obligatorio");
      }
  }

  //Inserción de datos.
    try {

        //Conexión con la BD

        $connection = App::getConnection();

        //Repositorios

        $categoryRepository = new CategoryRepository();
     
        if ($_SERVER["REQUEST_METHOD"]==="POST") {
          
        
            if (empty($errores)) { //Si no hay errores en el formulario, creamos una categoría
                $category = new Category(
                    htmlspecialchars($_POST["nombre"]), 
                );

                $categoryRepository->save($category); //Guardamos la categoría
            }
        }

        //Excepciones

    } catch (FileException $fileException) {
        $errores [] = $fileException->getMessage();
    } catch (AppException $appException) {
        $errores [] = $appException->getMessage();
    } catch (QueryException $queryException) {
        $errores [] = $queryException->getMessage();
    }

    //Vista
  
  require __DIR__ . "/../views/createCategory.view.php";