<?php
  require "utils/utils.php";
  require "repository/PostRepository.php";
  
  try {

    //Conexión con la BD

      $connection = App::getConnection();

      //Repositorios

      $postRepository = new PostRepository();

      //Consulta del post pasado por url

      $post = $postRepository->findById($_GET["id"]);

      //Excepciones

  } catch (AppException $appException) {
    $errores [] = $appException->getMessage();
  } catch (QueryException $queryException) {
    $errores [] = $queryException->getMessage();
  } 

  //Vista
  
  require __DIR__ . "/../views/post.view.php";
  