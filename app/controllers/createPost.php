<?php
  require "utils/utils.php";
  require "utils/File.php";
  require "repository/PostRepository.php";

  $errores = [];

  //Validación del formulario.
  if ($_SERVER['REQUEST_METHOD']==='POST') {
      if (empty($_POST["titulo"])) {
          array_push($errores, "El campo titulo es obligatorio");
      }

      if (empty($_POST["autor"])) {
          array_push($errores, "El campo autor es obligatorio");
      }
 
      if (empty($_POST["contenido"])) {
          array_push($errores, "El campo contenido es obligatorio");
      }

      if (empty($_POST["categoria"])) {
          array_push($errores, "El campo categoría es obligatorio");
      }
  }

  //Inserción de datos.
    try {

        //Conexión con la BD

        $connection = App::getConnection();

        //Repositorios

        $postRepository = new PostRepository();

        $categoryRepository = new CategoryRepository();

        $authorRepository = new AuthorRepository();

        //Consulta de los autores y las categorías

        $categories = $categoryRepository->findAll();

        $authors = $authorRepository->findAll();
     
        if ($_SERVER["REQUEST_METHOD"]==="POST") {
            if ($_FILES["imagen"]["name"] != null) { //Comprobamos si se ha enviado una imagen por el formulario
                $tiposAceptados = ["image/jpeg", "image/jpg", "image/png", "image/gif"];
                $imagen = new File("imagen", $tiposAceptados); //Creamos un objeto File con la imagen

                $imagen->moveUploadFile("uploads/"); //Movemos la imagen a la carpeta uploads

                $nombreImagen = $imagen->getFileName();
            }

            $contenido = htmlspecialchars($_POST["contenido"]); //Evitamos la inyección html en el contenido
        
            if (empty($errores)) { //Si no hay errores en el formulario, se crea un nuevo post
                $post = new Post(
                    htmlspecialchars($_POST["titulo"]), 
                    nl2br($contenido), //Con esta función mantenemos los saltos del línea del textarea mediante la generación de <br/>
                    $nombreImagen ?? null, //En caso de que no se haya enviado una imagen, se guarda como null
                    htmlspecialchars($_POST["autor"]), 
                    htmlspecialchars($_POST["categoria"])
                );

                $postRepository->save($post); //Guardamos el post
            }
        }

        //Excepciones

    } catch (FileException $fileException) {
        $errores [] = $fileException->getMessage();
    } catch (AppException $appException) {
        $errores [] = $appException->getMessage();
    } catch (QueryException $queryException) {
        $errores [] = $queryException->getMessage();
    }
  
    //Vista

  require __DIR__ . "/../views/createPost.view.php";
