<?php
  require "utils/utils.php";
  require "repository/PostRepository.php";


  try {

    //Conexión con la BD

      $connection = App::getConnection();

      //Repositorios

      $postRepository = new PostRepository();
      $categoryRepository = new CategoryRepository();

      //Si se pasa una categoría por la url, se consultan los posts que pertenecen a esa categoría

      if (isset($_GET["categoria"])) {
          $posts = $postRepository->findAll('where categoria="'.$_GET["categoria"].'"', 'order by fecha desc');
      } else {
          $posts = $postRepository->findAll('order by fecha desc');
      }

      //Consulta de las categorías

      $categories = $categoryRepository->findAll();

      //Excepciones

  } catch (AppException $appException) {
      $errores [] = $appException->getMessage();
  } catch (QueryException $queryException) {
    $errores [] = $queryException->getMessage();
  }

  //Vista

  require __DIR__ . "/../views/index.view.php";
  
