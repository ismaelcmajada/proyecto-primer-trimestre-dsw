<?php
  require "utils/utils.php";
  require "repository/MessageRepository.php";

  $errores = [];

      //Validación del formulario.

      if ($_SERVER['REQUEST_METHOD']==='POST') {
          if (empty($_POST["nombre"])) {
              array_push($errores, "El campo nombre es obligatorio");
          }

          if (empty($_POST["apellidos"])) {
              array_push($errores, "El campo apellidos es obligatorio");
          }

          if (empty($_POST["email"])) {
              array_push($errores, "El campo email es obligatorio");
          } elseif ((filter_var($_POST["email"], FILTER_VALIDATE_EMAIL))==false) {
              array_push($errores, "El campo email no es válido");
          }

          if (empty($_POST["asunto"])) {
              array_push($errores, "El campo asunto es obligatorio");
          }

          if (empty($_POST["mensaje"])) {
              array_push($errores, "El campo mensaje es obligatorio");
          }
      }

      //Inserción de datos

      try {

        //Conexión con la BD

        $connection = App::getConnection();

        //Repositorios

        $messageRepository = new MessageRepository();

          if ($_SERVER["REQUEST_METHOD"]==="POST") {
              
              if (empty($errores)) { //Si no hay errores en el formulario, creamos un objeto Message.
                $message = new Message(htmlspecialchars($_POST["nombre"]), htmlspecialchars($_POST["apellidos"]), htmlspecialchars($_POST["email"]), htmlspecialchars($_POST["asunto"]), htmlspecialchars($_POST["mensaje"]));
                
                $messageRepository->save($message); //Guardamos el objeto en la base de datos.
              }
          }

          //Excepciones

      } catch (AppException $appException) {
          $errores [] = $appException->getMessage();
      } catch (QueryException $queryException) {
          $errores [] = $queryException->getMessage();
      }

      //Vista

require __DIR__ . "/../views/contact.view.php";
