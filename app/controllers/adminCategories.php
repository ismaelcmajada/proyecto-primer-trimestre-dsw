<?php
  require "utils/utils.php";
  require "repository/CategoryRepository.php";

  try {

    //Conexión con la BD

      $connection = App::getConnection();

      //Repositorios

      $categoryRepository = new CategoryRepository();

      //Consulta de las categorías

      $categories = $categoryRepository->findAll();

      //Eliminar categorías por formulario

      if ($_SERVER['REQUEST_METHOD']==='POST') {
          if (isset($_POST['delete']) && isset($_POST['id'])) {

              $categoryRepository->deleteById($_POST['id']);

              header("Location: admincategories"); //Después de eliminar la categoría, recargamos el archivo.
          }
      }

      //Excepciones

  } catch (AppException $appException) {
    $errores [] = $appException->getMessage();
  } catch (QueryException $queryException) {
    $errores [] = $queryException->getMessage();
  }
  
  //Vista

  require __DIR__ . "/../views/adminCategories.view.php";