<?php
  require "utils/utils.php";
  require "utils/File.php";
  require "repository/CategoryRepository.php";

  $errores = [];

  //Validación del formulario.
  if ($_SERVER['REQUEST_METHOD']==='POST') {
    if (empty($_POST["nombre"])) {
        array_push($errores, "El campo nombre es obligatorio");
    }
  }

  //Modificación de datos
    try {

        //Conexión con la BD

        $connection = App::getConnection();

        //Repositorios

        $categoryRepository = new CategoryRepository();

        //Consulta de la categoría a modificar

        $category = $categoryRepository->findById($_GET["id"]);

        if ($_SERVER["REQUEST_METHOD"]==="POST") {
        
            if (empty($errores)) { //Si no hay errores en el formulario se crea una nueva categoría

                $categoryUpdate = new Category(
                    htmlspecialchars($_POST["nombre"]),
                );

                $categoryRepository->updateById($categoryUpdate, $_POST["id"]); //Se modifica la categoría con el id pasado por POST, usando las propiedades de la categoría que hemos creado

            }
        }

        //Excepciones

    } catch (FileException $fileException) {
        $errores [] = $fileException->getMessage();
    } catch (AppException $appException) {
        $errores [] = $appException->getMessage();
    } catch (QueryException $queryException) {
        $errores [] = $queryException->getMessage();
    }

    //Vista
  
  require __DIR__ . "/../views/modifyCategory.view.php";
