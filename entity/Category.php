<?php

require_once __DIR__ . "/../database/IEntity.php";

class Category implements IEntity {

    //Propiedades

    private $id;
    private $nombre;

    //Constructor

    public function __construct($nombre='') {
        $this->id = null;
        $this->nombre = $nombre;
    }

    //Métodos

    public function toArray(): array { //Devuelve los campos como un array asociativo.
        return [
            "nombre"=>$this->getNombre()          
        ];
    }

    //Getters y Setters

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }
}
?>