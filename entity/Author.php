<?php

require_once __DIR__ . "/../database/IEntity.php";

class Author implements IEntity {

    //Propiedades

    private $id;
    private $imagen;
    private $nombre;
    private $apellidos;
    private $descripcion;

    //Constructor

    public function __construct($imagen='', $nombre='', $apellidos='', $descripcion='') {
        $this->id = null;
        $this->imagen = $imagen;
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->descripcion = $descripcion;
    }

    //Métodos

    public function toArray(): array { //Devuelve los campos como un array asociativo.
        return [
            "imagen"=>$this->getImagen(),
            "nombre"=>$this->getNombre(),
            "apellidos"=>$this->getApellidos(),
            "descripcion"=>$this->getDescripcion()          
        ];
    }

    //Getters y Setters

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of imagen
     */ 
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set the value of imagen
     *
     * @return  self
     */ 
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of apellidos
     */ 
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set the value of apellidos
     *
     * @return  self
     */ 
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Get the value of descripcion
     */ 
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set the value of descripcion
     *
     * @return  self
     */ 
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }
}
?>