<?php

//En este archivo se almacenan métodos útiles para la aplicación

    //active, lo uso para comprobar si el string pasado por parámetro se encuentra en la uri, en este caso para el nav, para definir la página activa.

    function active($page) {
        if(strpos($_SERVER['REQUEST_URI'],$page)!=FALSE) {
            return true;
        } else {
            return false;
        }
    }

    //limitText, esta función me permite limitar un texto por número de palabras, la uso en el index, para limitar el texto de los post.

    function limitText($string, $num_words)
    {
        $words = array();
        $words = explode(" ", $string, $num_words);
        $shown_string = "";

        for ($i = 0; $i<count($words); $i++) {
            if ($words[$i]=="") {
                unset($words[$i]);
            }
        }

        if (count($words) == $num_words) {
            $words[$num_words-1] = "";
            $words[$num_words-2] = $words[$num_words-2]."...";
        }

        $shown_string = implode(" ",$words);

        return $shown_string;
    }
?>