<?php

    require_once __DIR__ . "/../exceptions/FileException.php";

    //Clase File, usada para la subida de archivos

    class File {

        //Propiedades

        private $file;
        private $fileName;

        //Constructor

        public function __construct(string $fileName, array $arrayTypes) {
            $this->file = $_FILES[$fileName];
            $this->fileName = $this->file["name"];

            //Comprobación del archivo

            if($this->file["name"] == "") {
                throw new FileException("No has enviado el archivo");
            }

            if($this->file["error"] !== UPLOAD_ERR_OK) {
                switch ($this->file["error"]) {
                    case UPLOAD_ERR_INI_SIZE:
                    
                    case UPLOAD_ERR_FORM_SIZE:
                        throw new FileException("Error de tamaño");

                    case UPLOAD_ERR_PARTIAL:
                        throw new FileException("Error de archivo incompleto");
                    default:
                        throw new FileException("Error genérico en la subida del archivo.");
                        break;
                }
            }

            if(in_array($this->file["type"], $arrayTypes)===false) {
                throw new FileException("Error de tipo de archivo");
            }
        }

        //Métodos

        /**
         * Get the value of fileName
         */ 
        public function getFileName()
        {
            return $this->fileName;
        }

        //Mueve el archivo a la ruta especificada

        public function moveUploadFile(string $ruta)
        {
            if (!is_uploaded_file($this->file["tmp_name"])) { //Comprueba si el archivo se ha enviado mediante formulario.
                throw new FileException("Error, el archivo no se ha subido mediante un formulario.");
            } 
            if (is_file($ruta.$this->file["name"])) { //Si el archivo ya existe, le concatena la fecha en milisegundos en el nombre
                $this->file["name"] = time()."_".$this->file["name"];
                $this->fileName = $this->file["name"];

            } 
            if (!move_uploaded_file($this->file["tmp_name"], $ruta.$this->file["name"])) { //Comprueba si el archivo se ha movido correctamente
                throw new FileException("Error, no se ha podido mover el archivo");
            }
        }
    }
?>