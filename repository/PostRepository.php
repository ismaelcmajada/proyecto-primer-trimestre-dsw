<?php
require_once __DIR__ . "/../entity/Post.php";
require_once __DIR__ . "/../database/QueryBuilder.php";
require_once __DIR__ . "/../repository/CategoryRepository.php";
require_once __DIR__ . "/../repository/AuthorRepository.php";

//Repositorio de la clase Post

class PostRepository extends QueryBuilder
{
    public function __construct(string $table="posts", string $classEntity="Post")
    {
        parent::__construct($table, $classEntity);
    }

    //Obtener la categoría del post

    public function getCategoria(Post $publicacion): Category
    {
        $categoryRepository = new CategoryRepository();

        return $categoryRepository->findbyId($publicacion->getCategoria() ?? 0);
    }

    //Obtener el autor del post

    public function getAutor(Post $publicacion): Author
    {
        $authorRepository = new AuthorRepository();

        return $authorRepository->findbyId($publicacion->getAutor() ?? 0);
    }
}
