<?php

require_once __DIR__ . "/../entity/Author.php";
require_once __DIR__ . "/../database/QueryBuilder.php";

//Repositorio de la clase Author

class AuthorRepository extends QueryBuilder
{
    public function __construct(string $table="authors", string $classEntity="Author")
    {
        parent::__construct($table, $classEntity);
    }
}
?>