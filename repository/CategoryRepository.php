<?php

require_once __DIR__ . "/../entity/Category.php";
require_once __DIR__ . "/../database/QueryBuilder.php";

//Repositorio de la clase Category

class CategoryRepository extends QueryBuilder
{
    public function __construct(string $table="categories", string $classEntity="Category")
    {
        parent::__construct($table, $classEntity);
    }
}
?>