<?php

require_once __DIR__ . "/../entity/Message.php";
require_once __DIR__ . "/../database/QueryBuilder.php";

//Repositorio de la clase Message

class MessageRepository extends QueryBuilder
{
    public function __construct(string $table="messages", string $classEntity="Message")
    {
        parent::__construct($table, $classEntity);
    }
}
?>