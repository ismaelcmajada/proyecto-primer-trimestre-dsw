<?php
require_once "App.php";
require_once "Request.php";

$config = require_once __DIR__ . "/../app/config.php"; //Obtenemos la configuración de la conexión

App::bind("config", $config); //Pasamos la configuración a la conexión accediendo al contenedor de la clase APP
?>