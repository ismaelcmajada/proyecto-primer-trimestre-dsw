<?php

require_once __DIR__ . "/../exceptions/AppException.php";
require_once __DIR__ . "/../database/Connection.php";

class App { //En esta clase, almacenamos en un array cosas importantes para la aplicación, como la conexión con la BD

    private static $container = []; //Contenedor

    public static function bind(string $key, $value) {
        static::$container[$key] = $value;
    }

    public static function get(string $key) { //Obtenemos del contenedor
        if(!array_key_exists($key, static::$container)) {
            throw new AppException("No se ha encontrado la clave $key en el contenedor.");
        }

        return static::$container[$key];
    }

    public static function getConnection() { //Podemos obtener una conexión 
        if(!array_key_exists("connection", static::$container)) {
            static::$container["connection"] = Connection::make();
        }

        return static::$container["connection"];
    }

}

?>